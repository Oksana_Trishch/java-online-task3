package com.gmail.okstrishch;

public class Eagle extends Bird {
    public Eagle(String name, String sleep, String eat) {
        super(name, sleep, eat);
    }

    void eat(){
        System.out.println("Eagle eats only meat");
    }
}
