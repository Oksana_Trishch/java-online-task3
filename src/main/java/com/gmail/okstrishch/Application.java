package com.gmail.okstrishch;

public class Application {
    public static void main(String[] args) {
        Bird bird = new Bird("Passer","I can sleep", "I can eat worm");
        System.out.println("My name is : "+bird.getName()+ " "+bird.getSleep()+ " and "+bird.getEat());
        bird.fly(" slowly!!!");
        Bird bird1 = new Bird("Sharm","I can sleep", "I can eat worm and some bread");
        System.out.println("My name is : "+bird1.getName()+ " "+bird1.getSleep()+ " and "+bird1.getEat());
        bird1.fly ();
        Fish fish = new Fish("Shark-Anakin", "I can sleep","I can eat a little fish");
        System.out.println("My name is : "+fish.getName()+ " "+fish.getSleep()+ " and "+fish.getEat());
        fish.swim();
        Parrot myParrot = new Parrot("Gesha", "I can sleep", "I can eat a little seed");
        System.out.println("My name is : "+myParrot.getName()+ " "+myParrot.getSleep()+ " and "+myParrot.getEat());
        myParrot.fly();
        Eagle myEagle = new Eagle("Strar", "I can not  sleep", "I can ear parrot and passer");
        System.out.println("My name is : "+myEagle.getName()+ " "+myEagle.getSleep()+ " and "+myEagle.getEat());
        myEagle.fly(" fast and quiet");


    }
}
