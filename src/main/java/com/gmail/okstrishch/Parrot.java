package com.gmail.okstrishch;


public class Parrot extends Bird {
    public Parrot(String name, String sleep, String eat) {
        super(name, sleep, eat);
    }

    void eat(){
        System.out.println("Parrot likes eating plants");
    }
//    void eat(String name){
//        super(name);
//                this.name = name;
//        System.out.println(name + " likes eating plants");
//    }
}
