package com.gmail.okstrishch;

public class Animal {
    private String name;
    private String sleep;
    private String eat;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSleep() {
        return sleep;
    }

    public void setSleep(String sleep) {
        this.sleep = sleep;
    }

    public String getEat() {
        return eat;
    }

    public void setEat(String eat) {
        this.eat = eat;
    }
    public Animal(String name, String sleep, String eat){
        this.name = name;
        this.sleep = sleep;
        this.eat = eat;
    }
}

