package com.gmail.okstrishch;

public class Bird extends Animal {
    public Bird(String name, String sleep, String eat ){
        super(name, sleep, eat);
    }
    void fly(){
        System.out.println("Bird's flying now");
    }
    void fly(String move){
        System.out.println("Bird's flying" + move );
    }
}
