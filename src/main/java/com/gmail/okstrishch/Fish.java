package com.gmail.okstrishch;

public class Fish extends Animal {
    public Fish(String name, String sleep, String eat ){
        super(name, sleep, eat);
    }
    void swim(){
        System.out.println("Fish swims every day");
    }
}
